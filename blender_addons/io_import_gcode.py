# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

## blendish python ripped from fro io_import_dxf

import bpy 
from bpy_extras.object_utils import object_data_add
from mathutils import Vector

bl_info = {
    'name': 'Import moves from Universal GCode (for 3D printer or CNC)',
    'author': 'Élie Michel',
    'version': (0,0,5),
    'blender': (2, 5, 6),
    'api': 32738,
    'location': 'File > Import-Export > Universal GCode',
    'description': 'Import and visualize GCode files generated for 3D printers or CNC machines (.gcode)',
    "wiki_url": "",
    "tracker_url": "",
    'category': 'Import-Export'}

__version__ = '.'.join([str(s) for s in bl_info['version']])

# gcode parser for blender 2.5 
# Simon Kirkby
# 201102051305
# tigger@interthingy.com 

#modified by David Anderson to handle Skeinforge comments
# Thanks Simon!!!

# modified by Alessandro Ranellucci (2011-10-14)
# to make it compatible with Blender 2.59
# and with modern 5D GCODE

# Modified by Winter Guerra (XtremD) on February 16th, 2012
# to make the script compatable with stock Makerbot GCode files
# and grab all nozzle extrusion information from Skeinforge's machine output
# WARNING: This script no longer works with stock 5D GCode! (Can somebody please integrate the two versions together?)

# A big shout-out goes to my friend Jon Spyreas for helping me block-out the maths needed in the "addArc" subroutine
# Thanks a million dude!
# Github branch link: https://github.com/xtremd/blender-gcode-reader

# Simplified by Élie Michel (exppad.com, github: @eliemichel) on October 16th, 2016
# to keep only the path and so make it much faster, though less featureful.
# See the original branch at https://github.com/zignig/blender-gcode-reader for more features

import string,os
import bpy
import mathutils
import math

class move():
    def __init__(self, args=[]):
        d = {'x': 0.0, 'y': 0.0, 'z': 0.0}
        for a in args:
            a = a.lower()
            for key in d.keys():
                if a.startswith(key):
                    try:
                        d[key] = float(a[1:])
                    except ValueError:
                        print("Warning: could not parse float in " + a)
        self.point = (d['x'], d['y'], d['z'])

class tool_on:
    def __init__(self, args=[]):
        pass

class tool_off:
    def __init__(self, args=[]):
        pass

codes = {
    'G0': move,
    'G1': move,
    'G01' : move,
    'M101' : tool_on,
    'M103' : tool_off,
}

class driver:
    # takes action object list and runs through it 
    def __init__(self):
        pass
    
    def drive(self):
        pass 
    
    def load_data(self,data):
        self.data = data

    
def vertsToPoints(Verts):
    # main vars
    vertArray = []
    for v in Verts:
        vertArray += v
        vertArray.append(0)
        #vertArray.append(1) #If using NURBS, must be 1
    return vertArray

def create_poly(verts, counter):
    name = 'skein' + str(counter) 
    
    verts = [Vector(p) for p in verts]
    edges = [(i, i + 1) for i in range(len(verts) - 1)]
    faces = []

    mesh = bpy.data.meshes.new(name=name)
    mesh.from_pydata(verts, edges, faces)
    obj = object_data_add(bpy.context, mesh)
    return obj
    
class blender_driver(driver):
     def __init__(self):
         driver.__init__(self)
         
     def drive(self):
        print('creating poly lines')
        poly = []
        endingPoint = (0, 0, 0)
        startingPoint = (0, 0, 0)
        layers = []
        this_layer = []
        counter = 1
        for act in self.data:
            if isinstance(act, move):
                poly.append(act.point)
                endingPoint = act.point
            
            if isinstance(act,tool_off):
                if len(poly) > 0:#A poly is only a poly if it has more than one point!
                    
                    poly.insert(0, startingPoint) #Prepend the poly with the last point before the extruder was turned on
                    
                    pobj = create_poly(poly,counter)
                        
                    this_layer.append(pobj)
                else:                       #This is not a poly! Discard!
                    print('Discarding bad poly')
                poly = []
                startingPoint = endingPoint
        layers.append(this_layer)


class GCodeParser:
    def __init__(self):
        self.commands = []
        
    def process(self, file_name):
        print('opening ' + file_name)
        f = open(file_name)
        for line in f:
            # cut comments out
            line = line.split(';')[0]

            tokens = line.split()
            if len(tokens) == 0:
                continue
            command = tokens[0]
            
            if command in codes:
                act = codes[command](tokens[1:])
                self.commands.append(act)
            else:
                print(line.strip())
                print('^' * len(command) + ' Unknown command')

        self.commands.append(tool_off())
        f.close()


def import_gcode(file_name):
    print('hola')
    p = GCodeParser()
    p.process(file_name)
    d = blender_driver()
    d.load_data(p.commands)
    d.drive()
    print('finished parsing... done')


DEBUG = False
from bpy.props import *

def tripleList(list1):
    list3 = []
    for elt in list1:
        list3.append((elt,elt,elt))
    return list3

theMergeLimit = 4
theCodec = 1 
theCircleRes = 1

class IMPORT_OT_gcode(bpy.types.Operator):
    '''Imports Universal GCode'''
    bl_idname = "import_scene.gocde"
    bl_description = 'GCode reader, reads tool moves and animates layer build'
    bl_label = "Import gcode" +' v.'+ __version__
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"

    filepath = StringProperty(name="File Path", description="Filepath used for importing the GCode file", maxlen= 1024, default= "")

    ##### DRAW #####
    def draw(self, context):
        layout0 = self.layout
        
    def execute(self, context):
        global toggle, theMergeLimit, theCodec, theCircleRes

        import_gcode(self.filepath)
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        wm.fileselect_add(self)
        return {'RUNNING_MODAL'}

def menu_func(self, context):
    self.layout.operator(IMPORT_OT_gcode.bl_idname, text="Universal GCode (.gcode)", icon='PLUGIN')

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_import.append(menu_func)
 
def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_import.remove(menu_func)

if __name__ == "__main__":
    register()

