import numpy as np
from scipy.ndimage import imread
from scipy.misc import imresize

# parameters
z_step = 0.6
total_thickness = 10
d = 2.5
source = '/home/elie/Images/scans/penny/zoom4/montage/final.jpg'
output = '/tmp/penny.gcode'
resize_factor = 0.02

# computed parameter
nb_layers = int(total_thickness / z_step)

# utils aliases
tr = np.transpose

# loading image
image = imread(source)
image = imresize(image, resize_factor)

# initializing output file
gcodefile = open(output, 'w')
gcodefile.write("""
; Header
G92

""")

def go(x, y, z, f=None):
    if f is not None:
        gcodefile.write("G1 %s Y%s Z%s F%s\n" % (x, y, z, f))
    else:
        gcodefile.write("G1 X%s Y%s Z%s\n" % (x, y, z))

# grid utils
l2r = range(image.shape[0])
r2l = range(image.shape[0] - 1, -1, -1)
b2f = range(image.shape[1])
f2b = range(image.shape[1] - 1, -1, -1)

# main loops
go(0, 0, 0)
for l in range(nb_layers):
    z_limit = total_thickness - l * z_step
    for i in l2r if l % 2 == 0 else r2l:
        for j in b2f if (i + l) % 2 == 0 else f2b:
            z_ideal = image[i,j] / 255.0 * total_thickness
            go(i * d, j * d, max(z_limit, z_ideal) - total_thickness)

print("Carving size (in mm): (%f, %f)" % (image.shape[0] * d, image.shape[1] * d))

