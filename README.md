CNC tools
=========

`image2gcode.py`: Converts a greyscale image into gcode instructions to carve it as a bas-relief. See the beginning of the script to change parameters.

TODO: Add other grid directions and types (e.g. radial). Follow iso-height line. Skip already carved areas.

`blender_addons/io_import_gcode.py`: Add-on for blender loading a gcode file as a simple edge-only object representing the trajectory of the carving head. Actually, it just reads G0/G1 instructions, so it also works for 3D-printer.

TODO: cut the object down into pieces to fasten loading.

